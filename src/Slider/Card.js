import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import './Card.scss';

export class Card extends React.Component {
    constructor(props) {
        super(props);
        this.cardContainerRef = React.createRef();
        this.isClose = false;
    }

    close = () => {
        this.isClose = true;
        if (this.cardContainerRef.current) {
            document.querySelector('.picture').style.left = this.props.left + 'px';
            document.querySelector('.picture').style.top = this.props.top + 'px';
        }
        this.props.close();
    };

    render() {
        setTimeout(() => {
            if (!this.isClose) {
                console.log(document.querySelector('.picture'));
                const el = document.querySelector('.picture');
                if (el) {
                    el.style.left = this.cardContainerRef.current.clientWidth * 0.2 + 'px';
                    el.style.top = this.cardContainerRef.current.clientHeight * 0.2 + 'px';
                }
            }
        }, 0);
        return (
            <div className="card-container" ref={this.cardContainerRef}>
                <Container>
                    <Row>
                        <Col
                            style={{
                                padding: 0,
                                border: '1px solid red',
                                position: 'absolute',
                                left: this.props.left,
                                top: this.props.top,
                                transition: 'all 1000ms ease-in-out',
                            }}
                            className="picture"
                        >
                            {this.props.children}
                        </Col>
                        <Col style={{ marginLeft: 250 }}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet aperiam assumenda
                            deserunt dicta doloremque excepturi facere iste laborum, magni minima molestiae mollitia
                            necessitatibus quod similique ullam unde voluptate voluptatibus.
                        </Col>
                        <Col>
                            В ролях:
                            <ul>
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                                <li>6</li>
                                <li>7</li>
                                <li>8</li>
                                <li>9</li>
                                <li>10</li>
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button
                                onClick={this.close}
                            >
                                Close
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
