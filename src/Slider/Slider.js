import React from 'react';
import ItemsCarousel from 'react-items-carousel';
import { OneSlide } from './FakeSlider/OneSlide';

export const range = (amount) => {
    return new Array(amount).fill(0).map((item, index) => index);
};

export class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeItemIndex: 0,
        };
    }

    changeActiveItem = (activeItemIndex) => {
        this.setState({ activeItemIndex });
    };

    render() {
        const { activeItemIndex } = this.state;
        return (
            <ItemsCarousel
                // Placeholder configurations
                enablePlaceholder
                numberOfPlaceholderItems={5}
                minimumPlaceholderTime={1000}
                placeholderItem={<div style={{ height: 200, background: '#900' }}>Placeholder</div>}

                // Carousel configurations
                numberOfCards={5}
                gutter={12}
                showSlither={false}
                firstAndLastGutter={true}
                freeScrolling={false}

                // Active item configurations
                requestToChangeActive={this.changeActiveItem}
                activeItemIndex={activeItemIndex}
                activePosition={'left'}

                chevronWidth={60}
                rightChevron={'>'}
                leftChevron={'<'}
                outsideChevron={true}
            >
                {
                    range(20)
                        .map((item) => (
                            <OneSlide key={item} index={item} setActive={this.changeActiveItem}/>
                        ))
                }
            </ItemsCarousel>
        );
    }
}
