import React, { Fragment } from 'react';
import Slick from 'react-slick';
import './SlickSlider.scss';
import { range } from '../Slider';

export class SlickSlider extends React.Component {

    constructor(props) {
        super(props);
        this.sliderElementRef = React.createRef();
    }

    goNext = (event) => {
        event.preventDefault();
        this.sliderElementRef.current.slickNext();
    };

    goPrev = (event) => {
        event.preventDefault();
        this.sliderElementRef.current.slickPrev();
    };

    goToIndex = (index) => (event) => {
        event.preventDefault();
        if (index !== undefined) {
            this.sliderElementRef.current.slickGoTo(index);
        }
    };

    render() {
        const settings = {
            dots: false,
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            focusOnSelect: true,
        };
        const { children } = this.props;
        return (
            <Fragment>
                <Slick ref={this.sliderElementRef} {...settings}>
                    {
                        children.map((child, index) => (
                            <div
                                key={'slick_slide_' + index}
                            >
                                {child}
                            </div>
                        ))
                    }
                </Slick>
                <a href="#" className="arrow-btn btn-prev" onClick={this.goPrev}>{'<'}</a>
                {
                    range(children.length)
                        .map((item, index) => (
                            <a key={`dot-${index}`} href={`#slide-${index}`} className="dot" onClick={this.goToIndex(index)}>•</a>
                        ))
                }
                <a href="#" className="arrow-btn btn-next" onClick={this.goNext}>{'>'}</a>
            </Fragment>
        );
    }
}
