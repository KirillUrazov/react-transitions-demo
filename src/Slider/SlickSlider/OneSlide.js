import React, { Fragment } from 'react';

export class OneSlide extends React.Component {

    static defaultProps = {
        index: null,
        showCard: () => {},
    };

    constructor(props) {
        super(props);
        this.state = {
            isShowCard: false,
            isShowPreview: true,
        };
        this.slideRef = React.createRef();
    }

    showCard = () => {
        console.log(this.props.index);
        this.props.showCard(this.slideRef);
    };

    render() {
        const { index } = this.props;
        const { isShowCard, isShowPreview } = this.state;
        return (
            <Fragment>
                <div
                    style={{
                        backgroundImage: 'url(https://via.placeholder.com/150x300)',
                        height: 300,
                        width: 150,
                        margin: '0 auto',
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'contain',
                    }}
                    className="text-center"
                    onClick={this.showCard}
                    ref={this.slideRef}
                >
                    <h3>{index}</h3>
                </div>
            </Fragment>
        );
    }
}
