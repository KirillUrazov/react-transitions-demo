import React from 'react';
import CSSTransition from 'react-transition-group/esm/CSSTransition';
import { Card } from '../Card';

let tmpStyle = {};
const enteringStyle = {
    position: 'relative',
    left: 0,
    top: 0,
    transition: 'all 500ms ease-in-out',
};
const exitingStyle = {
    position: 'relative',
    left: 'auto',
    top: 'auto',
    transition: 'all 500ms ease-in-out',
};

export class OneSlide extends React.Component {
    static defaultProps = {
        setActive: () => {
        },
        index: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            isShowCard: false,
            isShowPreview: true,
        };
        this.slideRef = React.createRef();
    }

    showCard = () => {
        if (this.slideRef.current) {
        const sliderListElement = this.slideRef.current.closest('.fake-slider-list');
            this.left = this.slideRef.current.offsetLeft - sliderListElement.scrollLeft;
            this.top = this.slideRef.current.offsetTop - sliderListElement.scrollTop;
        }
        console.log(this.state.isShowCard);
        this.setState({ isShowCard: true, isShowPreview: false }, () => {
            console.log(this.state.isShowCard);
        });
    };

    closeCard = () => {
        console.log(this.state.isShowCard);
        this.setState({ isShowCard: false }, () => {
            console.log(this.state.isShowCard);
        });
    };

    render() {
        const { isShowCard, isShowPreview } = this.state;
        const { index } = this.props;
        return (
            <div style={{ width: 150 }}>
                {
                    !isShowCard && isShowPreview &&
                    <div
                        style={{
                            backgroundImage: 'url(https://via.placeholder.com/150x300)',
                            height: 300,
                            width: 150,
                            margin: '0 auto',
                            backgroundPosition: 'center',
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: 'contain',
                        }}
                        className="text-center"
                        onClick={this.showCard}
                        ref={this.slideRef}
                    >
                        <h3>{index}</h3>
                    </div>
                }
                <CSSTransition
                    in={isShowCard}
                    timeout={1000}
                    classNames="card-item"
                    unmountOnExit
                    onExiting={() => {
                        document.querySelector('.picture').style.transform = `translate(${this.left}, ${this.top})`;
                        // document.querySelector('.picture').style.transition = 'all 3000ms ease-in-out';
                        // document.querySelector('.picture').style.left = this.left;
                        // document.querySelector('.picture').style.top = this.top;
                    }}
                    onExited={() => {
                        this.setState({ isShowPreview: true });
                    }}
                >
                    <Card close={this.closeCard} left={this.left} top={this.top}>
                        <div
                            style={{
                                backgroundImage: 'url(https://via.placeholder.com/150x300)',
                                height: '100%',
                                backgroundPosition: 'center',
                                backgroundRepeat: 'no-repeat',
                                backgroundSize: 'cover',
                            }}
                            className="text-center"
                            onClick={this.showCard}
                            ref={this.slideRef}
                        >
                            <h3>{index}</h3>
                        </div>
                    </Card>
                </CSSTransition>
            </div>
        );
    }
}

/*
<img src="https://via.placeholder.com/150" alt="150" onClick={this.showCard}/>
 */

/*
            <div>
                <div
                    style={{
                        backgroundImage: 'url(https://via.placeholder.com/150)',
                        height: 150,
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'contain',
                        zIndex: 15,
                    }}
                    className="text-center"
                    onClick={this.showCard}
                >
                    <h3>{index}</h3>
                </div>
            </div>
 */
