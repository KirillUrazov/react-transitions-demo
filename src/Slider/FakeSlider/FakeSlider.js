import React from 'react';
import { range } from '../Slider';
import './FakeSlider.scss';

export class FakeSlider extends React.Component {
    static defaultProps = {
        numberOfShowSlide: 5,
    };

    sliderListRef = React.createRef();

    goToSlideByIndex = (index) => () => {
        console.log(`Scroll to ${index} slide`);
    };

    goToNext = (e) => {
        e.preventDefault();
        console.log('Scroll to next slide');
        this.sliderListRef.current.scrollBy({
            left: 250
        });
    };

    goToPrev = (e) => {
        e.preventDefault();
        console.log('Scroll to prev slide');
        this.sliderListRef.current.scrollBy({
            left: -250
        });
    };

    render() {
        const { children, numberOfShowSlide } = this.props;
        const marginAroundSlide = 100 / numberOfShowSlide / 2 / 2;
        return (
            <div className="fake-slider-container">
                <div className="fake-slider-list" ref={this.sliderListRef}>
                    {children.map((Child, index) => (
                        <div
                            key={`slide-${index}`}
                            style={{
                                marginLeft: (index !== 0) ? `${marginAroundSlide}%` : '0',
                                marginRight: ((children.length - 1) !== index) ? `${marginAroundSlide}%` : '0',
                            }}
                            id={`slide-${index}`}
                        >
                            {Child}
                        </div>
                    ))}
                </div>
                <a href="#" className="arrow-btn btn-prev" onClick={this.goToPrev}>{'<'}</a>
                {
                    range(children.length)
                        .map((item, index) => (
                            <a key={`dot-${index}`} href={`#slide-${index}`} className="dot" onClick={this.goToSlideByIndex(index)}>•</a>
                        ))
                }
                <a href="#" className="arrow-btn btn-next" onClick={this.goToNext}>{'>'}</a>
            </div>
        );
    }
}
