import React, { Fragment } from 'react';
import './App.scss';
import Container from 'react-bootstrap/Container';
import { FakeSlider } from '../Slider/FakeSlider/FakeSlider';
import { range } from '../Slider/Slider';
import { OneSlide } from '../Slider/SlickSlider/OneSlide';
import Jumbotron from 'react-bootstrap/Jumbotron';
import { SlickSlider } from '../Slider/SlickSlider/SlickSlider';
import { Card } from '../Slider/Card';
import CSSTransition from 'react-transition-group/cjs/CSSTransition';

export class App extends React.Component {

    state = {
        isShowCard: false,
    };

    showCard = (slideRef) => {
        console.log(slideRef.current);
        this.setState({ isShowCard: true });
    };

    // todo затемняем фон, оставляем прозрачный блок под фотку
    //  (как-то это можно сделать через css, главное узнать размеры и позицию фотки в слайдере)
    //  и выводим текст карточки.
    //  сделать чере CSSTransition
    render() {
        const {isShowCard} = this.state;
        return (
            <Fragment>
                <Container>
                    <Jumbotron />
                </Container>
                <Container fluid={true} className="slider-container my-5">
                    <FakeSlider>
                        {
                            range(10)
                                .map((item) => (
                                    <OneSlide key={item} index={item} />
                                ))
                        }
                    </FakeSlider>
                </Container>
                <Container className="my-5">
                    {/*<Slider/>*/}
                </Container>
                <Container className="my-5">
                    <SlickSlider>
                        {
                            range(10)
                                .map((item) => (
                                    <OneSlide key={'slick_slide_' + item} index={item} showCard={this.showCard} />
                                ))
                        }
                    </SlickSlider>
                </Container>
                {/*{*/}
                {/*    isShowCard &&*/}
                {/*    (*/}
                {/*        <CSSTransition>*/}
                {/*        </CSSTransition>*/}
                {/*    )*/}
                {/*}*/}
            </Fragment>
        );
    }
}
